from django.urls import path

from posts.views import list_all_posts, show_post_detail, create_post, edit_post

urlpatterns = [
    path("<int:pk>/", show_post_detail, name="post_detail"),
    path("", list_all_posts, name="post_list"),
    path("create/", create_post, name="post_create"),
    path("edit/<int:pk>/", edit_post, name="post_edit"),
]

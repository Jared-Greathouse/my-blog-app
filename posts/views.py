from django.shortcuts import get_object_or_404, render

from .models import Post


# Create your views here.
def show_post_detail(request, pk):
    post = Post.objects.get(pk=pk)
    context = {"post": post}
    return render(request, "posts/detail.html", context)


def list_all_posts(request):
    # 1.  get the data
    #  - use the django model methods to get the data
    posts = Post.objects.all()

    # 2. pass the data to the template
    # make a context dictionary with "posts" as the key and all the posts from the database as the value
    context = {"posts": posts}

    # 3. return the template to the browser
    # return an http response by returning the django render function, which calls the template and gives it the data in the context
    return render(request, "posts/list.html", context)


def create_post(request):
    pass
    # if request.method = "POST" and PostForm:
    #     form =


def edit_post(request, pk):
    pass
